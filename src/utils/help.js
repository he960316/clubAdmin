// 格式化数字

export function fmtNumber(val) {
  if (val > 9) {
    return val;
  } else {
    return "0" + val;
  }
}

export const fmtDate = function (date) {
  return `${fmtNumber(date.getFullYear())}-${fmtNumber(
    date.getMonth() + 1
  )}-${fmtNumber(date.getDate())} ${fmtNumber(date.getHours())}:${fmtNumber(
    date.getMinutes()
  )}`;
};
