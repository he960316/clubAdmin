// 节流函数
export default function throttle(fn, gapTime = 1500) {
  // 第一次调用
  let isFirst = true;

  let timer = null;

  return function () {
    if (isFirst) {
      isFirst = false;
      fn.apply(this, arguments);
      return;
    }
    if (timer) {
      return;
    }
    timer = setTimeout(() => {
      clearInterval(this.timer);
      timer = null;
      fn.apply(this, arguments);
    }, gapTime);
  };
}
