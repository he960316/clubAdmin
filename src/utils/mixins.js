import { mapMutations } from "vuex";
export default {
  data() {
    return {
      docmHeight: document.documentElement.clientHeight, // 默认屏幕高度
      showHeight: document.documentElement.clientHeight, // 实时屏幕高度
    };
  },
  mounted() {
    this.$nextTick(() => {
      window.onresize = () => {
        return (() => {
          this.showHeight = document.body.clientHeight;
        })();
      };
    });
  },
  computed: {
    hideShow() {
      return this.showHeight >= this.docmHeight;
    },
  },
  methods: {
    ...mapMutations("publish", ["setStateByKey"]),
  },
};
