import { Toast, Indicator } from "mint-ui"; // 提示
import axios from "axios";
// import configs from "../configs";
import store from "../store";

let real_api = "";

if (window.location.host.indexOf("dev") !== -1) {
  real_api = "https://dev.popbadminton.com";
} else if (window.location.host.indexOf("test") !== -1) {
  real_api = "https://preview.popbadminton.com";
} else if (window.location.host.indexOf("preview") !== -1) {
  real_api = "https://preview.popbadminton.com";
} else if (window.location.host.indexOf("localhost") !== -1) {
  real_api = "https://dev.popbadminton.com";
} else {
  real_api = "https://api.popbadminton.com";
  // real_api = "https://test.popbadminton.com";
}

const service = axios.create({
  // baseURL: process.env.VUE_APP_BASE_API, // url = base url + request url
  baseURL: real_api,
  headers: {
    "Access-Control-Allow-Headers": "openid",
    "Content-Type": "application/json;",
    // 'content-type': 'application/x-www-form-urlencoded',
    // 这些参数需要从小程序传递过来
    // token: ,
    // openid: ,
    // iv: iv,
    // encryptedData: encryptedData,
    // code: code
  },
  // withCredentials: true, // send cookies when cross-domain requests
  timeout: 5000, // request timeout
});
// 添加请求拦截器
service.interceptors.request.use(
  function (config) {
    console.log(config);

    config.headers.token = sessionStorage.getItem("token") || configs.token;
    config.headers.openid = sessionStorage.getItem("openid") || configs.openId;
    // 在发送请求之前做些什么
    // 登陆路径不刷新token
    Indicator.open({
      spinnerType: "fading-circle",
    });
    return config;
  },
  function (error) {
    // 对请求错误做些什么
    return Promise.reject(error);
  }
);

// 添加响应拦截器
service.interceptors.response.use(
  function ({ status, data }) {
    Indicator.close();
    if (status === 200 && data.code === 200) {
      return Promise.resolve(data.data);
    } else {
      if (data.code === 206) {
        store.commit("setBlack", true);
        return;
      }
      console.error(data.code, status);
      let message = "请求失败！";
      if (data && data.msg) {
        message = data.msg;
      }
      Toast({
        message,
        position: "middle",
        duration: 2000,
      });

      return Promise.reject(new Error(message));
    }
  },
  function (error) {
    Indicator.close();
    return Promise.reject(error);
  }
);

export default service;
