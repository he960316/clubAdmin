import Vue from "vue";
import VueRouter from "vue-router";
import Home from "../views/Home.vue";
import Store from "../store";

Vue.use(VueRouter);

const routes = [
  {
    path: "/",
    name: "Home",
    component: Home,
    meta: {
      title: "home",
    },
  },
  {
    path: "/soldCardSetting",
    name: "SoldCardSetting",
    component: () =>
      import(/* webpackChunkName: "about" */ "../views/SoldCardSetting.vue"),
    meta: {
      title: "售卡设置",
    },
    props: (route) => ({ clubId: route.query.clubId }),
  },
  {
    path: "/circulation",
    name: "circulation",
    component: () =>
      import(/* webpackChunkName: "bill" */ "../views/circulation.vue"),
    meta: {
      title: "小循环",
    },
    props: (route) => ({ id: route.query.id, userid: route.query.userid }),
  },
  {
    path: "/assetsStatistic",
    name: "AssetsStatistic",
    component: () =>
      import(/* webpackChunkName: "bill" */ "../views/AssetsStatistic.vue"),
    meta: {
      title: "资产统计",
    },
    props: (route) => ({ clubId: route.query.clubId }),
  },
  {
    path: "/soldCardDetail",
    name: "SoldCardDetail",
    component: () =>
      import(/* webpackChunkName: "about" */ "../views/SoldCardDetail.vue"),
    meta: {
      title: "卡片详情",
    },
  },
  {
    path: "/paymentDetail",
    name: "PaymentDetail",
    component: () =>
      import(
        /* webpackChunkName: "PaymentDetail" */ "../views/PaymentDetail.vue"
      ),
    meta: {
      title: "收支明细",
    },
    props: (route) => ({ clubId: route.query.clubId }),
  },
  {
    path: "/cashObtain",
    name: "CashObtain",
    component: () =>
      import(/* webpackChunkName: "bill" */ "../views/CashObtain.vue"),
    meta: {
      title: "提现",
    },
  },
  {
    path: "/addCardForSold",
    name: "AddCardForSold",
    component: () =>
      import(/* webpackChunkName: "about" */ "../views/AddCardForSold.vue"),
    meta: {
      title: "添加售卡",
    },
  },
  {
    path: "/addCardComment",
    name: "AddCardComment",
    component: () =>
      import(/* webpackChunkName: "about" */ "../views/AddCardComment.vue"),
    meta: {
      title: "补充说明",
    },
  },
  {
    path: "/selectMemberForAddCard/:clubId/:type/:amount",
    name: "SelectMemberForAddCard",
    component: () =>
      import(
        /* webpackChunkName: "about" */ "../views/SelectMemberForAddCard.vue"
      ),
    meta: {
      title: "添加开卡会员",
    },
  },
  {
    path: "/cardManage",
    name: "CardManage",
    component: () =>
      import(/* webpackChunkName: "about" */ "../views/CardManage.vue"),
    meta: {
      title: "俱乐部卡管理",
    },
  },
  {
    path: "/billDetail",
    name: "BillDetail",
    component: () =>
      import(/* webpackChunkName: "bill" */ "../views/BillDetail.vue"),
    meta: {
      title: "费用明细",
    },
    props: (route) => ({
      aId: route.query.aId,
      clubId: route.query.clubId,
      type: route.query.type,
    }),
  },
  {
    path: "/userYearCardDetail",
    name: "UserYearCardDetail",
    component: () =>
      import(/* webpackChunkName: "bill" */ "../views/UserYearCardDetail.vue"),
    meta: {
      title: "修改年卡信息",
    },
  },
  {
    path: "/ApplyManager",
    name: "ApplyManager",
    component: () =>
      import(/* webpackChunkName: "bill" */ "../views/ApplyManager.vue"),
    meta: {
      title: "已报名成员",
    },
    props: (route) => ({
      aId: route.query.id,
      clubId: route.query.clubId,
      number: route.query.number,
      isManager: route.query.isManager,
      isScoring: route.query.isScoring,
      hidden: route.query.hidden,
    }),
  },
  {
    path: "/addUserYearCard",
    name: "AddUserYearCard",
    component: () =>
      import(/* webpackChunkName: "bill" */ "../views/AddUserYearCard.vue"),
    meta: {
      title: "开通会员年卡",
    },
  },
  {
    path: "/addUserRechargeCard",
    name: "AddUserRechargeCard",
    component: () =>
      import(/* webpackChunkName: "bill" */ "../views/AddUserRechargeCard.vue"),
    meta: {
      title: "开通会员充值卡",
    },
  },
  {
    path: "/editUserRechargeCard",
    name: "EditUserRechargeCard",
    component: () =>
      import(
        /* webpackChunkName: "bill" */ "../views/EditUserRechargeCard.vue"
      ),
    meta: {
      title: "修改充值卡金额",
    },
  },
  {
    path: "/cardOrderManage",
    name: "CardOrderManage",
    component: () =>
      import(/* webpackChunkName: "about" */ "../views/CardOrderManage.vue"),
    meta: {
      title: "订单管理",
    },
  },
  {
    path: "/permissionSetting",
    name: "PermissionSetting",
    component: () =>
      import(/* webpackChunkName: "about" */ "../views/PermissionSetting.vue"),
    meta: {
      title: "权限设置",
    },
  },
  {
    path: "/rightsManagement",
    name: "RightsManagement",
    component: () =>
      import(/* webpackChunkName: "about" */ "../views/RightsManagement.vue"),
    meta: {
      title: "设置权限",
    },
    props: (route) => ({
      userId: route.query.userId,
      clubId: route.query.clubId,
      userIds: route.query.userIds,
    }),
  },
  {
    path: "/selectMemberForPermissionSetting/:clubId",
    name: "SelectMemberForPermissionSetting",
    component: () =>
      import(
        /* webpackChunkName: "about" */ "../views/SelectMemberForPermissionSetting.vue"
      ),
    meta: {
      title: "选择执行理事",
    },
  },
  {
    path: "/publishActivity",
    name: "publishActivity",
    component: () =>
      import(/* webpackChunkName: "publish" */ "../views/PublishActivity.vue"),
    meta: {
      title: "混合活动",
      keepAlive: true,
    },
    props: (route) => ({
      clubId: route.query.clubId,
      commentData: route.query.comment,
      userData: route.query.user,
      clubData: route.query.club,
      tem_aId: route.query.aId,
      state: route.query.state,
    }),
  },
  {
    path: "/assignActivity",
    name: "AssignActivity",
    component: () =>
      import(/* webpackChunkName: "publish" */ "../views/AssignActivity.vue"),
    meta: {
      title: "分配活动",
    },
    props: (route) => ({
      clubId: route.query.clubId,
      aId: route.query.aId,
    }),
  },
  {
    path: "/selectGym",
    name: "SelectGym",
    component: () =>
      import(/* webpackChunkName: "publish" */ "../views/SelectGym.vue"),
    meta: {
      title: "选择场馆",
    },
    props: (route) => ({
      clubId: route.query.clubId,
      aId: route.query.aId,
    }),
  },
  {
    path: "/selectActivityAdmin",
    name: "SelectActivityAdmin",
    component: () =>
      import(
        /* webpackChunkName: "publish" */ "../views/SelectActivityAdmin.vue"
      ),
    meta: {
      title: "选择组织者",
    },
    props: (route) => ({
      clubId: route.query.clubId,
      aId: route.query.aId,
    }),
  },
  {
    path: "/activityComment",
    name: "ActivityComment",
    component: () =>
      import(/* webpackChunkName: "publish" */ "../views/ActivityComment.vue"),
    meta: {
      title: "活动说明",
    },
    props: (route) => ({
      clubId: route.query.clubId,
      aId: route.query.aId,
    }),
  },

  {
    path: "/recordDetailForMatchPlay",
    name: "RecordDetailForMatchPlay",
    component: () =>
      import(
        /* webpackChunkName: "publish" */ "../views/RecordDetailForMatchPlay.vue"
      ),
    meta: {
      title: "对抗赛",
    },
    props: (route) => ({
      userId: route.query.userId,
      aId: route.query.aId,
    }),
  },
];

const router = new VueRouter({
  mode: "hash",
  base: process.env.BASE_URL,
  routes,
  scrollBehavior(to, from, savedPosition) {
    if (savedPosition && to.name == "PaymentDetail") {
      return savedPosition;
    }
    return { x: 0, y: 0 };
  },
});

router.beforeEach((to, from, next) => {
  if (to.query.token) {
    sessionStorage.setItem("token", to.query.token);
  }
  if (to.query.openid) {
    sessionStorage.setItem("openid", to.query.openid);
  }

  if (to.query.clubId) {
    Store.commit("bill/setClubId", to.query.clubId);
  }
  if (to.meta.title) {
    document.title = to.meta.title;
  }
  next();
});

export default router;
