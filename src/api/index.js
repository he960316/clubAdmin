/**
 *
 * api配置
 */

export default {
  getCardList: "/api/card/getCardList",
  getCardInfo: "/api/card/getCardInfo",
  clubBill: "/api/Club/clubBill",
  setCardDefault: "/api/card/setCardDefault",
  DelCard: "/api/card/DelCard",
  addCardType: "/api/card/addCardType",
  cashObtain: "/api/pay/Cash_withdrawal_record",
  publishCash: "/api/pay/Cash_withdrawal",
  getBill: "/api/club/clubAllBill",
  clubMember: "/api/Club/clubMember",
  getCardedUserList: "/api/card/getCardedUserList",
  billDetail: "/api/club/BillDetail",
  editGenerationOrderAmount: "/api/order/editGenerationOrderAmount",
  editCardOrderAmount: "api/order/editCardOrderAmount",
  getUserCardInfo: "api/card/getUserCardInfo",
  createUserCard: "/api/card/createUserCard",
  updateUserCard: "/api/card/updateUserCard",
  refundUserCard: "/api/card/refundUserCard",
  cardManage: "/api/card/cardManage",
  clubDetail: "/api/club/clubDetail",
  signOutClub: "/api/Club/signOutClub",
  dealOrderRecharge: "api/card/dealOrderRecharge",
  setPermission: "/api/club/setPermission",
  getPermission: "/api/club/getPermission",
  getClubIdentityStatus: "/api/user/getClubIdentityStatus",
  activityMemberList: "/Api/ActivityMember/list",
  getActivityOPtions: "/api/activity/getOption",
  createMixedActivity: "/api/activity/createMixedActivity",
  getDefaultMixedActivityData: "/api/activity/getDefaultMixedActivityData",
  updateMixedActivity: "/api/activity/updateMixedActivity",
  getActivtyConfigScene: "/api/Activity/getActivtyConfigScene",
  mixedActivtyAssignPeople: "api/ActivityMember/mixedActivtyAssignPeople",
  recordInfo: "/Api/user/recordInfo", // 战绩详情
  searchVenues: "/api/Venues/searchVenues", // 搜索场馆
};
