export default {
  namespaced: true,
  state: {
    activitySettings: null,
    priceData: null,
    otherSettings: null,
    user: null,
    club: null,
    comment: "",
    isEdit: false,
    timeChange: false,
  },
  getters: {
    activitySettings(state) {
      return state.activitySettings;
    },
    priceData(state) {
      return state.priceData;
    },
    otherSettings(state) {
      return state.otherSettings;
    },
    user(state) {
      return state.user;
    },
    club(state) {
      return state.club;
    },
    comment(state) {
      return state.comment;
    },
    isEdit(state) {
      return state.isEdit;
    },
    timeChange(state) {
      return state.timeChange;
    },
  },
  mutations: {
    setStateByKey(state, payload) {
      for (const key in payload) {
        if (payload.hasOwnProperty(key)) {
          state[key] = payload[key];
        }
      }
    },
    setTimeChange(state, payload) {
      state.timeChange = payload;
    },
  },
  actions: {},
};
