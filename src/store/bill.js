export default {
  namespaced: true,
  state: {
    clubId: "1194",
  },
  getters: {
    clubId(state) {
      return state.clubId;
    },
  },
  mutations: {
    setClubId(state, clubId) {
      state.clubId = clubId;
    },
  },
  actions: {},
};
