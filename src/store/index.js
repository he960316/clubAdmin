import Vue from "vue";
import Vuex from "vuex";
import bill from "./bill";
import publish from "./publish";

Vue.use(Vuex);

const store = new Vuex.Store({
  state: {
    activities: [
      {
        id: 2,
        name: "自由活动",
        bgImg:
          "https://popbadminton.oss-cn-shanghai.aliyuncs.com/micro_pop/system1.png",
        desc: "活动时间内，自由搭档对抗。",
        isNewest: false,
        rule_type: "type2",
        optionPeople: null,
        isShow: true,
      },
      {
        id: 4,
        name: "小乱斗",
        bgImg:
          "https://popbadminton.oss-cn-shanghai.aliyuncs.com/micro_pop/system4.png",
        desc:
          "随机搭档进行多轮次双打比赛，根据场地情况可采用不同分制，以个人成绩计算名次。",
        isNewest: true,
        rule_type: "type4",
        isShow: true,
        temOrder: 1,
        optionPeople: {
          min: 6,
          max: 16,
          default: 12,
          step: 1,
        },
      },
      {
        id: 9,
        name: "大乱斗",
        bgImg:
          "https://popbadminton.oss-cn-shanghai.aliyuncs.com/micro_pop/096/system8%402x.png",
        desc:
          "随机匹配搭档和对手的双打活动，轮次不限，以个人净胜场、净胜分、总得分进行排名。",
        isNewest: true,
        rule_type: "type9",
        isShow: true,
        temOrder: 2,
        optionPeople: {
          // min: 6,
          // max: 40,
          // default: 12,
          // step: 1
          min: 6,
          max: 40,
          // 默认等于0代表手动输入
          defaultNum: 8,
        },
      },
      {
        id: 6,
        name: "个人升级赛",
        bgImg:
          "https://popbadminton.oss-cn-shanghai.aliyuncs.com/micro_pop/self.jpg",
        desc:
          "随机搭档进行多轮次双打比赛，每人每胜一局积1分，同分者继续随机对抗，以个人成绩计算名次。",
        isNewest: true,
        rule_type: "type6",
        isShow: true,
        temOrder: 3,
        optionPeople: {
          min: 16,
          max: 32,
          default: 16,
          step: 16,
        },
      },
      {
        id: 0,
        name: "小循环",
        bgImg:
          "https://popbadminton.oss-cn-shanghai.aliyuncs.com/micro_pop/circle.png",
        desc:
          "以双打组合进行比赛，每对组合与其他组合依次轮流进行对抗，根据胜场、净胜分计算名次。",
        isNewest: false,
        rule_type: "type0",
        isShow: true,
        temOrder: 4,
        optionPeople: {
          min: 10,
          max: 20,
          default: 10,
          step: 2,
        },
      },
      {
        id: 8,
        name: "淘汰赛",
        bgImg:
          "https://popbadminton.oss-cn-shanghai.aliyuncs.com/micro_pop/095/system7.png",
        desc:
          "以24人为例，12对选手可分为2或4个小组先进行小循环，各组的1、2名出线后再进行交叉对抗。",
        isNewest: false,
        rule_type: "type8",
        isShow: true,
        temOrder: 5,
        optionPeople: {
          min: 12,
          max: 48,
          default: 12,
          step: 2,
        },
      },
      {
        // 目前不显示，显示的时候修改活动信息
        id: 10,
        name: "排位赛",
        bgImg:
          "https://popbadminton.oss-cn-shanghai.aliyuncs.com/micro_pop/096/system7%402x.png",
        desc:
          "以双打组合进行比赛，每对组合与其他组合依次轮流进行对抗，根据胜场、净胜分计算名次。",
        isNewest: false,
        rule_type: "type10",
        isShow: false,
        // 由于新建活动的顺序和临时活动的顺序不一致，这个是临时活动中的排序，新建活动按照当前数组的顺序。
        temOrder: 6,
      },
      {
        id: 5,
        name: "对抗赛",
        bgImg:
          "https://popbadminton.oss-cn-shanghai.aliyuncs.com/micro_pop/system3.png",
        desc:
          "A、B两队所有组合依次轮流进行双打比赛，同队无对抗，以获胜总场次、总净胜分计算胜负。",
        isNewest: false,
        rule_type: "type5",
        isShow: true,
        temOrder: 7,
        optionPeople: {
          min: 12,
          max: 40,
          default: 12,
          step: 4,
        },
      },
      {
        id: 7,
        name: "南北大战",
        bgImg:
          "https://popbadminton.oss-cn-shanghai.aliyuncs.com/micro_pop/south_north.png",
        desc:
          "A、B两队进行多轮次双打比赛，队内随机匹配搭档形成每轮对阵，以获胜总场次、总净胜分计算胜负。",
        isNewest: true,
        rule_type: "type7",
        isShow: true,
        temOrder: 8,
        optionPeople: {
          min: 8,
          // 默认等于0代表手动输入
          defaultNum: 12,
        },
        totalTurns: {
          min: 2,
          max: 10,
          default: 4,
          step: 1,
        },
      },
      {
        id: 11,
        name: "混合活动",
        bgImg:
          "https://popbadminton.oss-cn-shanghai.aliyuncs.com/micro_pop/south_north.png",
        desc:
          "A、B两队进行多轮次双打比赛，队内随机匹配搭档形成每轮对阵，以获胜总场次、总净胜分计算胜负。",
        isNewest: true,
      },
      {
        id: 12,
        name: "训练场",
        bgImg:
          "https://popbadminton.oss-cn-shanghai.aliyuncs.com/micro_pop/south_north.png",
        desc:
          "A、B两队进行多轮次双打比赛，队内随机匹配搭档形成每轮对阵，以获胜总场次、总净胜分计算胜负。",
        isNewest: true,
      },
    ],
    isBlack: false,
  },
  getters: {
    activities(state) {
      return state.activities;
    },
    isBlack(state) {
      return state.isBlack;
    },
  },
  mutations: {
    setBlack(state, payload) {
      state.isBlack = payload;
    },
  },
  actions: {},
  modules: {
    bill,
    publish,
  },
});

export default store;
